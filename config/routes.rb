Rails.application.routes.draw do


  root 'soon#soon'
  get 'pages/home'
  get 'pages/test'
  get 'pages/about'
  get 'home/about'
  get 'home/test'
  resources :blogs
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
